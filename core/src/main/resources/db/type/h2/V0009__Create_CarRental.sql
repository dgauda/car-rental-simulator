    CREATE TABLE CarRentalInfo ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,     
      address VARCHAR(1000),
      zipcode BIGINT,
      totalNoOfCars BIGINT,
      availableNoOfCars BIGINT,
      operatingcompany VARCHAR(1000),
      CONSTRAINT PK_CarRentalInfo PRIMARY KEY(id)     
    );  

    CREATE TABLE RentalCarStatus ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,
      name VARCHAR(255),
      desc VARCHAR(2000),
      CONSTRAINT PK_RentalCarStatus PRIMARY KEY(id)      
    );
    
    CREATE TABLE CarRentalService ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,
      carDetail VARCHAR(200) NOT NULL,
      idCarRentalInfo BIGINT NOT NULL,
      idCarStatus BIGINT NOT NULL,
      CONSTRAINT PK_CarRentalService PRIMARY KEY(id),
      CONSTRAINT FK_CarRentalService_idCarRentalInfo FOREIGN KEY(idCarRentalInfo) REFERENCES CarRentalInfo(id) NOCHECK,
      CONSTRAINT FK_CarRentalService_idCarStatus FOREIGN KEY(idCarStatus) REFERENCES RentalCarStatus(id) NOCHECK
    );    
    

    CREATE TABLE CarReservation ( 
      id BIGINT NOT NULL AUTO_INCREMENT,  
      modificationCounter INTEGER NOT NULL,
      idCarRentalInfo BIGINT,
      idCarRentalService BIGINT,
      itineraryId BIGINT,
      startTimestamp TIMESTAMP,
      endTimestamp TIMESTAMP,
      CONSTRAINT PK_CarReservation PRIMARY KEY(id),
      CONSTRAINT FK_CarReservation_idCarRentalInfo FOREIGN KEY(idCarRentalInfo) REFERENCES CarRentalInfo(id) NOCHECK,
      CONSTRAINT FK_CarReservation_idCarRentalService FOREIGN KEY(idCarRentalService) REFERENCES CarRentalService(id) NOCHECK
    );
    
    