INSERT INTO CarRentalInfo (id, modificationCounter, address, zipcode, totalNoOfCars, availableNoOfCars, operatingcompany) VALUES (1, 0, 'WhiteField Car Rental, Bangalore', 1234567, 10, 9, 'vehicles on Rent');

INSERT INTO RentalCarStatus (id, modificationCounter, name , desc) VALUES (1, 0, 'Available', 'Car Available');
INSERT INTO RentalCarStatus (id, modificationCounter, name , desc) VALUES (2, 0, 'Booked', 'Not Available');

INSERT INTO CarRentalService (id, modificationCounter, carDetail, idCarRentalInfo, idCarStatus) VALUES (1, 0, 'Honda', 1, 1);
INSERT INTO CarRentalService (id, modificationCounter, carDetail, idCarRentalInfo, idCarStatus) VALUES (2, 0, 'Amaze', 1, 1);
INSERT INTO CarRentalService (id, modificationCounter, carDetail, idCarRentalInfo, idCarStatus) VALUES (3, 0, 'Xcent', 1, 1);
INSERT INTO CarRentalService (id, modificationCounter, carDetail, idCarRentalInfo, idCarStatus) VALUES (4, 0, 'Jazz', 1, 1);
INSERT INTO CarRentalService (id, modificationCounter, carDetail, idCarRentalInfo, idCarStatus) VALUES (5, 0, 'WagonR', 1, 1);
INSERT INTO CarRentalService (id, modificationCounter, carDetail, idCarRentalInfo, idCarStatus) VALUES (6, 0, 'Indigo', 1, 1);
INSERT INTO CarRentalService (id, modificationCounter, carDetail, idCarRentalInfo, idCarStatus) VALUES (7, 0, 'Ertiga', 1, 1);
INSERT INTO CarRentalService (id, modificationCounter, carDetail, idCarRentalInfo, idCarStatus) VALUES (8, 0, 'Tata Nano', 1, 1);
INSERT INTO CarRentalService (id, modificationCounter, carDetail, idCarRentalInfo, idCarStatus) VALUES (9, 0, 'Tata Tiago', 1, 1);
INSERT INTO CarRentalService (id, modificationCounter, carDetail, idCarRentalInfo, idCarStatus) VALUES (10, 0, 'Brezza', 1, 1);

INSERT INTO CarReservation (id, modificationCounter, idCarRentalInfo, idCarRentalService, itineraryId, startTimestamp, endTimestamp) VALUES (1, 0, 1, 3, 123, CURRENT_TIMESTAMP + (60 * 60 * 24 * 5), CURRENT_TIMESTAMP + (60 * 60 * 24 * 5));
