package com.cap.simulatorservices.carrental.dataaccess.api;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cap.simulatorservices.carrental.common.api.CarReservation;
import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author naveengu
 */
@Entity
@Table(name = "CarReservation")
public class CarReservationEntity extends ApplicationPersistenceEntity implements CarReservation {

  private Long itineraryId;

  @Temporal(TemporalType.TIMESTAMP)
  private Timestamp startTimestamp;

  @Temporal(TemporalType.TIMESTAMP)
  private Timestamp endTimestamp;

  private CarRentalInfoEntity carRentalInfo;

  private CarRentalServiceEntity carRentalService;

  private static final long serialVersionUID = 1L;

  /**
   * @return itineraryId
   */
  public Long getItineraryId() {

    return this.itineraryId;
  }

  /**
   * @param itineraryId new value of {@link #getitineraryId}.
   */
  public void setItineraryId(Long itineraryId) {

    this.itineraryId = itineraryId;
  }

  /**
   * @return startTimestamp
   */
  public Timestamp getStartTimestamp() {

    return this.startTimestamp;
  }

  /**
   * @param startTimestamp new value of {@link #getstartTimestamp}.
   */
  public void setStartTimestamp(Timestamp startTimestamp) {

    this.startTimestamp = startTimestamp;
  }

  /**
   * @return endTimestamp
   */
  public Timestamp getEndTimestamp() {

    return this.endTimestamp;
  }

  /**
   * @param endTimestamp new value of {@link #getendTimestamp}.
   */
  public void setEndTimestamp(Timestamp endTimestamp) {

    this.endTimestamp = endTimestamp;
  }

  /**
   * @return carRentalInfo
   */
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "idCarRentalInfo")
  public CarRentalInfoEntity getCarRentalInfo() {

    return this.carRentalInfo;
  }

  /**
   * @param carRentalInfo new value of {@link #getcarRentalInfo}.
   */
  public void setCarRentalInfo(CarRentalInfoEntity carRentalInfo) {

    this.carRentalInfo = carRentalInfo;
  }

  /**
   * @return carRentalService
   */
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "idCarRentalService")
  public CarRentalServiceEntity getCarRentalService() {

    return this.carRentalService;
  }

  /**
   * @param carRentalService new value of {@link #getcarRentalService}.
   */
  public void setCarRentalService(CarRentalServiceEntity carRentalService) {

    this.carRentalService = carRentalService;
  }

  @Override
  @Transient
  public Long getCarRentalInfoId() {

    if (this.carRentalInfo == null) {
      return null;
    }
    return this.carRentalInfo.getId();
  }

  @Override
  public void setCarRentalInfoId(Long carRentalInfoId) {

    if (carRentalInfoId == null) {
      this.carRentalInfo = null;
    } else {
      CarRentalInfoEntity carRentalInfoEntity = new CarRentalInfoEntity();
      carRentalInfoEntity.setId(carRentalInfoId);
      this.carRentalInfo = carRentalInfoEntity;
    }
  }

  @Override
  @Transient
  public Long getCarRentalServiceId() {

    if (this.carRentalService == null) {
      return null;
    }
    return this.carRentalService.getId();
  }

  @Override
  public void setCarRentalServiceId(Long carRentalServiceId) {

    if (carRentalServiceId == null) {
      this.carRentalService = null;
    } else {
      CarRentalServiceEntity carRentalServiceEntity = new CarRentalServiceEntity();
      carRentalServiceEntity.setId(carRentalServiceId);
      this.carRentalService = carRentalServiceEntity;
    }
  }

}
