package com.cap.simulatorservices.carrental.logic.api.to;

import com.cap.simulatorservices.carrental.common.api.CarRentalInfo;
import com.cap.simulatorservices.general.common.api.to.AbstractEto;

/**
 * Entity transport object of CarRentalInfo
 */
public class CarRentalInfoEto extends AbstractEto implements CarRentalInfo {

  private static final long serialVersionUID = 1L;

  private String address;

  private Long zipcode;

  private Long totalNoOfCars;

  private Long availableNoOfCars;

  private String operatingcompany;

  @Override
  public String getAddress() {

    return address;
  }

  @Override
  public void setAddress(String address) {

    this.address = address;
  }

  @Override
  public Long getZipcode() {

    return zipcode;
  }

  @Override
  public void setZipcode(Long zipcode) {

    this.zipcode = zipcode;
  }

  @Override
  public Long getTotalNoOfCars() {

    return totalNoOfCars;
  }

  @Override
  public void setTotalNoOfCars(Long totalNoOfCars) {

    this.totalNoOfCars = totalNoOfCars;
  }

  @Override
  public Long getAvailableNoOfCars() {

    return availableNoOfCars;
  }

  @Override
  public void setAvailableNoOfCars(Long availableNoOfCars) {

    this.availableNoOfCars = availableNoOfCars;
  }

  @Override
  public String getOperatingcompany() {

    return operatingcompany;
  }

  @Override
  public void setOperatingcompany(String operatingcompany) {

    this.operatingcompany = operatingcompany;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.address == null) ? 0 : this.address.hashCode());
    result = prime * result + ((this.zipcode == null) ? 0 : this.zipcode.hashCode());
    result = prime * result + ((this.totalNoOfCars == null) ? 0 : this.totalNoOfCars.hashCode());
    result = prime * result + ((this.availableNoOfCars == null) ? 0 : this.availableNoOfCars.hashCode());
    result = prime * result + ((this.operatingcompany == null) ? 0 : this.operatingcompany.hashCode());

    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    CarRentalInfoEto other = (CarRentalInfoEto) obj;
    if (this.address == null) {
      if (other.address != null) {
        return false;
      }
    } else if (!this.address.equals(other.address)) {
      return false;
    }
    if (this.zipcode == null) {
      if (other.zipcode != null) {
        return false;
      }
    } else if (!this.zipcode.equals(other.zipcode)) {
      return false;
    }
    if (this.totalNoOfCars == null) {
      if (other.totalNoOfCars != null) {
        return false;
      }
    } else if (!this.totalNoOfCars.equals(other.totalNoOfCars)) {
      return false;
    }
    if (this.availableNoOfCars == null) {
      if (other.availableNoOfCars != null) {
        return false;
      }
    } else if (!this.availableNoOfCars.equals(other.availableNoOfCars)) {
      return false;
    }
    if (this.operatingcompany == null) {
      if (other.operatingcompany != null) {
        return false;
      }
    } else if (!this.operatingcompany.equals(other.operatingcompany)) {
      return false;
    }

    return true;
  }
}
