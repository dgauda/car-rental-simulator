package com.cap.simulatorservices.carrental.dataaccess.api;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.cap.simulatorservices.carrental.common.api.CarRentalService;
import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author naveengu
 */
@Entity
@Table(name = "CarRentalService")
public class CarRentalServiceEntity extends ApplicationPersistenceEntity implements CarRentalService {

  private CarRentalInfoEntity carRentalInfo;

  private String carDetail;

  private RentalCarStatusEntity carStatus;

  private static final long serialVersionUID = 1L;

  /**
   * @return carRentalInfo
   */
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "idCarRentalInfo")
  public CarRentalInfoEntity getCarRentalInfo() {

    return this.carRentalInfo;
  }

  /**
   * @param carRentalInfo new value of {@link #getcarRentalInfo}.
   */
  public void setCarRentalInfo(CarRentalInfoEntity carRentalInfo) {

    this.carRentalInfo = carRentalInfo;
  }

  /**
   * @return carDetail
   */
  public String getCarDetail() {

    return this.carDetail;
  }

  /**
   * @param carDetail new value of {@link #getcarDetail}.
   */
  public void setCarDetail(String carDetail) {

    this.carDetail = carDetail;
  }

  /**
   * @return carStatus
   */
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "idCarStatus")
  public RentalCarStatusEntity getCarStatus() {

    return this.carStatus;
  }

  /**
   * @param carStatus new value of {@link #getcarStatus}.
   */
  public void setCarStatus(RentalCarStatusEntity carStatus) {

    this.carStatus = carStatus;
  }

  @Override
  @Transient
  public Long getCarRentalInfoId() {

    if (this.carRentalInfo == null) {
      return null;
    }
    return this.carRentalInfo.getId();
  }

  @Override
  public void setCarRentalInfoId(Long carRentalInfoId) {

    if (carRentalInfoId == null) {
      this.carRentalInfo = null;
    } else {
      CarRentalInfoEntity carRentalInfoEntity = new CarRentalInfoEntity();
      carRentalInfoEntity.setId(carRentalInfoId);
      this.carRentalInfo = carRentalInfoEntity;
    }
  }

  @Override
  @Transient
  public Long getCarStatusId() {

    if (this.carStatus == null) {
      return null;
    }
    return this.carStatus.getId();
  }

  @Override
  public void setCarStatusId(Long carStatusId) {

    if (carStatusId == null) {
      this.carStatus = null;
    } else {
      RentalCarStatusEntity rentalCarStatusEntity = new RentalCarStatusEntity();
      rentalCarStatusEntity.setId(carStatusId);
      this.carStatus = rentalCarStatusEntity;
    }
  }

}
