package com.cap.simulatorservices.carrental.logic.api.to;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link com.cap.simulatorservices.carrental.common.api.CarRentalService}s.
 *
 */
public class CarRentalServiceSearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private Long carRentalInfoId;

  private String carDetail;

  private Long carStatusId;

  /**
   * The constructor.
   */
  public CarRentalServiceSearchCriteriaTo() {

    super();
  }

  public Long getCarRentalInfoId() {

    return carRentalInfoId;
  }

  public void setCarRentalInfoId(Long carRentalInfoId) {

    this.carRentalInfoId = carRentalInfoId;
  }

  public String getCarDetail() {

    return carDetail;
  }

  public void setCarDetail(String carDetail) {

    this.carDetail = carDetail;
  }

  public Long getCarStatusId() {

    return carStatusId;
  }

  public void setCarStatusId(Long carStatusId) {

    this.carStatusId = carStatusId;
  }

}
