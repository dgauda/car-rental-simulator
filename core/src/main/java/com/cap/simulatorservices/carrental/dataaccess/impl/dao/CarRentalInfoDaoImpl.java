package com.cap.simulatorservices.carrental.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.carrental.dataaccess.api.CarRentalInfoEntity;
import com.cap.simulatorservices.carrental.dataaccess.api.dao.CarRentalInfoDao;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link CarRentalInfoDao}.
 */
@Named
public class CarRentalInfoDaoImpl extends ApplicationDaoImpl<CarRentalInfoEntity> implements CarRentalInfoDao {

  /**
   * The constructor.
   */
  public CarRentalInfoDaoImpl() {

    super();
  }

  @Override
  public Class<CarRentalInfoEntity> getEntityClass() {

    return CarRentalInfoEntity.class;
  }

  @Override
  public PaginatedListTo<CarRentalInfoEntity> findCarRentalInfos(CarRentalInfoSearchCriteriaTo criteria) {

    CarRentalInfoEntity carrentalinfo = Alias.alias(CarRentalInfoEntity.class);
    EntityPathBase<CarRentalInfoEntity> alias = Alias.$(carrentalinfo);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String address = criteria.getAddress();
    if (address != null) {
      query.where(Alias.$(carrentalinfo.getAddress()).eq(address));
    }
    Long zipcode = criteria.getZipcode();
    if (zipcode != null) {
      query.where(Alias.$(carrentalinfo.getZipcode()).eq(zipcode));
    }
    Long totalNoOfCars = criteria.getTotalNoOfCars();
    if (totalNoOfCars != null) {
      query.where(Alias.$(carrentalinfo.getTotalNoOfCars()).eq(totalNoOfCars));
    }
    Long availableNoOfCars = criteria.getAvailableNoOfCars();
    if (availableNoOfCars != null) {
      query.where(Alias.$(carrentalinfo.getAvailableNoOfCars()).eq(availableNoOfCars));
    }
    String operatingcompany = criteria.getOperatingcompany();
    if (operatingcompany != null) {
      query.where(Alias.$(carrentalinfo.getOperatingcompany()).eq(operatingcompany));
    }
    addOrderBy(query, alias, carrentalinfo, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<CarRentalInfoEntity> alias, CarRentalInfoEntity carrentalinfo,
      List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "address":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carrentalinfo.getAddress()).asc());
            } else {
              query.orderBy(Alias.$(carrentalinfo.getAddress()).desc());
            }
            break;
          case "zipcode":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carrentalinfo.getZipcode()).asc());
            } else {
              query.orderBy(Alias.$(carrentalinfo.getZipcode()).desc());
            }
            break;
          case "totalNoOfCars":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carrentalinfo.getTotalNoOfCars()).asc());
            } else {
              query.orderBy(Alias.$(carrentalinfo.getTotalNoOfCars()).desc());
            }
            break;
          case "availableNoOfCars":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carrentalinfo.getAvailableNoOfCars()).asc());
            } else {
              query.orderBy(Alias.$(carrentalinfo.getAvailableNoOfCars()).desc());
            }
            break;
          case "operatingcompany":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carrentalinfo.getOperatingcompany()).asc());
            } else {
              query.orderBy(Alias.$(carrentalinfo.getOperatingcompany()).desc());
            }
            break;
        }
      }
    }
  }

}