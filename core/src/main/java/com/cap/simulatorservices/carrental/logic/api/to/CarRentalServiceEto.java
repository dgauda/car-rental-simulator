package com.cap.simulatorservices.carrental.logic.api.to;

import com.cap.simulatorservices.carrental.common.api.CarRentalService;
import com.cap.simulatorservices.general.common.api.to.AbstractEto;

/**
 * Entity transport object of CarRentalService
 */
public class CarRentalServiceEto extends AbstractEto implements CarRentalService {

  private static final long serialVersionUID = 1L;

  private Long carRentalInfoId;

  private String carDetail;

  private Long carStatusId;

  @Override
  public Long getCarRentalInfoId() {

    return carRentalInfoId;
  }

  @Override
  public void setCarRentalInfoId(Long carRentalInfoId) {

    this.carRentalInfoId = carRentalInfoId;
  }

  @Override
  public String getCarDetail() {

    return carDetail;
  }

  @Override
  public void setCarDetail(String carDetail) {

    this.carDetail = carDetail;
  }

  @Override
  public Long getCarStatusId() {

    return carStatusId;
  }

  @Override
  public void setCarStatusId(Long carStatusId) {

    this.carStatusId = carStatusId;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();

    result = prime * result + ((this.carRentalInfoId == null) ? 0 : this.carRentalInfoId.hashCode());
    result = prime * result + ((this.carDetail == null) ? 0 : this.carDetail.hashCode());

    result = prime * result + ((this.carStatusId == null) ? 0 : this.carStatusId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    CarRentalServiceEto other = (CarRentalServiceEto) obj;

    if (this.carRentalInfoId == null) {
      if (other.carRentalInfoId != null) {
        return false;
      }
    } else if (!this.carRentalInfoId.equals(other.carRentalInfoId)) {
      return false;
    }
    if (this.carDetail == null) {
      if (other.carDetail != null) {
        return false;
      }
    } else if (!this.carDetail.equals(other.carDetail)) {
      return false;
    }

    if (this.carStatusId == null) {
      if (other.carStatusId != null) {
        return false;
      }
    } else if (!this.carStatusId.equals(other.carStatusId)) {
      return false;
    }
    return true;
  }
}
