package com.cap.simulatorservices.carrental.common.api;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface RentalCarStatus extends ApplicationEntity {

  public String getName();

  public void setName(String name);

  public String getDesc();

  public void setDesc(String desc);

}
