package com.cap.simulatorservices.carrental.logic.api.to;

import java.util.Set;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of CarRentalInfo
 */
public class CarRentalInfoCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private CarRentalInfoEto carRentalInfo;

  private Set<CarRentalServiceEto> carRentalService;

  public CarRentalInfoEto getCarRentalInfo() {

    return carRentalInfo;
  }

  public void setCarRentalInfo(CarRentalInfoEto carRentalInfo) {

    this.carRentalInfo = carRentalInfo;
  }

  public Set<CarRentalServiceEto> getCarRentalService() {

    return carRentalService;
  }

  public void setCarRentalService(Set<CarRentalServiceEto> carRentalService) {

    this.carRentalService = carRentalService;
  }

}
