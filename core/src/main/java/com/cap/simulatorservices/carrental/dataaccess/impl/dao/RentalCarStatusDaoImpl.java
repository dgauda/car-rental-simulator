package com.cap.simulatorservices.carrental.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.carrental.dataaccess.api.RentalCarStatusEntity;
import com.cap.simulatorservices.carrental.dataaccess.api.dao.RentalCarStatusDao;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link RentalCarStatusDao}.
 */
@Named
public class RentalCarStatusDaoImpl extends ApplicationDaoImpl<RentalCarStatusEntity> implements RentalCarStatusDao {

  /**
   * The constructor.
   */
  public RentalCarStatusDaoImpl() {

    super();
  }

  @Override
  public Class<RentalCarStatusEntity> getEntityClass() {

    return RentalCarStatusEntity.class;
  }

  @Override
  public PaginatedListTo<RentalCarStatusEntity> findRentalCarStatuss(RentalCarStatusSearchCriteriaTo criteria) {

    RentalCarStatusEntity rentalcarstatus = Alias.alias(RentalCarStatusEntity.class);
    EntityPathBase<RentalCarStatusEntity> alias = Alias.$(rentalcarstatus);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String name = criteria.getName();
    if (name != null) {
      query.where(Alias.$(rentalcarstatus.getName()).eq(name));
    }
    String desc = criteria.getDesc();
    if (desc != null) {
      query.where(Alias.$(rentalcarstatus.getDesc()).eq(desc));
    }
    addOrderBy(query, alias, rentalcarstatus, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<RentalCarStatusEntity> alias,
      RentalCarStatusEntity rentalcarstatus, List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "name":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(rentalcarstatus.getName()).asc());
            } else {
              query.orderBy(Alias.$(rentalcarstatus.getName()).desc());
            }
            break;
          case "desc":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(rentalcarstatus.getDesc()).asc());
            } else {
              query.orderBy(Alias.$(rentalcarstatus.getDesc()).desc());
            }
            break;
        }
      }
    }
  }

}