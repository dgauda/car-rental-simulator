package com.cap.simulatorservices.carrental.dataaccess.api;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.cap.simulatorservices.carrental.common.api.CarRentalInfo;
import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author naveengu
 */
@Entity
@Table(name = "CarRentalInfo")
public class CarRentalInfoEntity extends ApplicationPersistenceEntity implements CarRentalInfo {

  private String address;

  private Long zipcode;

  private Long totalNoOfCars;

  private Long availableNoOfCars;

  private String operatingcompany;

  private Set<CarRentalServiceEntity> carRentalService;

  private static final long serialVersionUID = 1L;

  /**
   * @return address
   */
  public String getAddress() {

    return this.address;
  }

  /**
   * @param address new value of {@link #getaddress}.
   */
  public void setAddress(String address) {

    this.address = address;
  }

  /**
   * @return zipcode
   */
  public Long getZipcode() {

    return this.zipcode;
  }

  /**
   * @param zipcode new value of {@link #getzipcode}.
   */
  public void setZipcode(Long zipcode) {

    this.zipcode = zipcode;
  }

  /**
   * @return totalNoOfCars
   */
  public Long getTotalNoOfCars() {

    return this.totalNoOfCars;
  }

  /**
   * @param totalNoOfCars new value of {@link #gettotalNoOfCars}.
   */
  public void setTotalNoOfCars(Long totalNoOfCars) {

    this.totalNoOfCars = totalNoOfCars;
  }

  /**
   * @return availableNoOfCars
   */
  public Long getAvailableNoOfCars() {

    return this.availableNoOfCars;
  }

  /**
   * @param availableNoOfCars new value of {@link #getavailableNoOfCars}.
   */
  public void setAvailableNoOfCars(Long availableNoOfCars) {

    this.availableNoOfCars = availableNoOfCars;
  }

  /**
   * @return operatingcompany
   */
  public String getOperatingcompany() {

    return this.operatingcompany;
  }

  /**
   * @param operatingcompany new value of {@link #getoperatingcompany}.
   */
  public void setOperatingcompany(String operatingcompany) {

    this.operatingcompany = operatingcompany;
  }

  /**
   * @return carRentalService
   */

  @OneToMany(mappedBy = "carRentalInfo", fetch = FetchType.EAGER)
  public Set<CarRentalServiceEntity> getCarRentalService() {

    return this.carRentalService;
  }

  /**
   * @param carRentalService new value of {@link #getcarRentalService}.
   */
  public void setCarRentalService(Set<CarRentalServiceEntity> carRentalService) {

    this.carRentalService = carRentalService;
  }

}
