package com.cap.simulatorservices.carrental.dataaccess.api.dao;

import com.cap.simulatorservices.carrental.dataaccess.api.CarRentalServiceEntity;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.api.dao.ApplicationDao;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for CarRentalService entities
 */
public interface CarRentalServiceDao extends ApplicationDao<CarRentalServiceEntity> {

  /**
   * Finds the {@link CarRentalServiceEntity carrentalservices} matching the given
   * {@link CarRentalServiceSearchCriteriaTo}.
   *
   * @param criteria is the {@link CarRentalServiceSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link CarRentalServiceEntity} objects.
   */
  PaginatedListTo<CarRentalServiceEntity> findCarRentalServices(CarRentalServiceSearchCriteriaTo criteria);
}
