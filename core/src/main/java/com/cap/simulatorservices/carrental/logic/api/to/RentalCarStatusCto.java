package com.cap.simulatorservices.carrental.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of RentalCarStatus
 */
public class RentalCarStatusCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private RentalCarStatusEto rentalCarStatus;

  public RentalCarStatusEto getRentalCarStatus() {

    return rentalCarStatus;
  }

  public void setRentalCarStatus(RentalCarStatusEto rentalCarStatus) {

    this.rentalCarStatus = rentalCarStatus;
  }

}
