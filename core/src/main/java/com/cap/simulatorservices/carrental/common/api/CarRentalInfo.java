package com.cap.simulatorservices.carrental.common.api;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface CarRentalInfo extends ApplicationEntity {

  public String getAddress();

  public void setAddress(String address);

  public Long getZipcode();

  public void setZipcode(Long zipcode);

  public Long getTotalNoOfCars();

  public void setTotalNoOfCars(Long totalNoOfCars);

  public Long getAvailableNoOfCars();

  public void setAvailableNoOfCars(Long availableNoOfCars);

  public String getOperatingcompany();

  public void setOperatingcompany(String operatingcompany);

}
