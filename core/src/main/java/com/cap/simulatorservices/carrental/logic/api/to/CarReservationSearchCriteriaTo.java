package com.cap.simulatorservices.carrental.logic.api.to;

import java.sql.Timestamp;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link com.cap.simulatorservices.carrental.common.api.CarReservation}s.
 *
 */
public class CarReservationSearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private Long itineraryId;

  private Timestamp startTimestamp;

  private Timestamp endTimestamp;

  private Long carRentalInfoId;

  private Long carRentalServiceId;

  /**
   * The constructor.
   */
  public CarReservationSearchCriteriaTo() {

    super();
  }

  public Long getItineraryId() {

    return this.itineraryId;
  }

  public void setItineraryId(Long itineraryId) {

    this.itineraryId = itineraryId;
  }

  public Timestamp getStartTimestamp() {

    return this.startTimestamp;
  }

  public void setStartTimestamp(Timestamp startTimestamp) {

    this.startTimestamp = startTimestamp;
  }

  public Timestamp getEndTimestamp() {

    return this.endTimestamp;
  }

  public void setEndTimestamp(Timestamp endTimestamp) {

    this.endTimestamp = endTimestamp;
  }

  public Long getCarRentalInfoId() {

    return this.carRentalInfoId;
  }

  public void setCarRentalInfoId(Long carRentalInfoId) {

    this.carRentalInfoId = carRentalInfoId;
  }

  public Long getCarRentalServiceId() {

    return this.carRentalServiceId;
  }

  public void setCarRentalServiceId(Long carRentalServiceId) {

    this.carRentalServiceId = carRentalServiceId;
  }

}
