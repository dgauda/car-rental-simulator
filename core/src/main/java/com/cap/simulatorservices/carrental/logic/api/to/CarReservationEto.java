package com.cap.simulatorservices.carrental.logic.api.to;

import java.sql.Timestamp;

import com.cap.simulatorservices.carrental.common.api.CarReservation;
import com.cap.simulatorservices.general.common.api.to.AbstractEto;

/**
 * Entity transport object of CarReservation
 */
public class CarReservationEto extends AbstractEto implements CarReservation {

  private static final long serialVersionUID = 1L;

  private Long itineraryId;

  private Timestamp startTimestamp;

  private Timestamp endTimestamp;

  private Long carRentalInfoId;

  private Long carRentalServiceId;

  @Override
  public Long getItineraryId() {

    return this.itineraryId;
  }

  @Override
  public void setItineraryId(Long itineraryId) {

    this.itineraryId = itineraryId;
  }

  @Override
  public Timestamp getStartTimestamp() {

    return this.startTimestamp;
  }

  @Override
  public void setStartTimestamp(Timestamp startTimestamp) {

    this.startTimestamp = startTimestamp;
  }

  @Override
  public Timestamp getEndTimestamp() {

    return this.endTimestamp;
  }

  @Override
  public void setEndTimestamp(Timestamp endTimestamp) {

    this.endTimestamp = endTimestamp;
  }

  @Override
  public Long getCarRentalInfoId() {

    return this.carRentalInfoId;
  }

  @Override
  public void setCarRentalInfoId(Long carRentalInfoId) {

    this.carRentalInfoId = carRentalInfoId;
  }

  @Override
  public Long getCarRentalServiceId() {

    return this.carRentalServiceId;
  }

  @Override
  public void setCarRentalServiceId(Long carRentalServiceId) {

    this.carRentalServiceId = carRentalServiceId;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.itineraryId == null) ? 0 : this.itineraryId.hashCode());
    result = prime * result + ((this.startTimestamp == null) ? 0 : this.startTimestamp.hashCode());
    result = prime * result + ((this.endTimestamp == null) ? 0 : this.endTimestamp.hashCode());

    result = prime * result + ((this.carRentalInfoId == null) ? 0 : this.carRentalInfoId.hashCode());

    result = prime * result + ((this.carRentalServiceId == null) ? 0 : this.carRentalServiceId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    CarReservationEto other = (CarReservationEto) obj;
    if (this.itineraryId == null) {
      if (other.itineraryId != null) {
        return false;
      }
    } else if (!this.itineraryId.equals(other.itineraryId)) {
      return false;
    }
    if (this.startTimestamp == null) {
      if (other.startTimestamp != null) {
        return false;
      }
    } else if (!this.startTimestamp.equals(other.startTimestamp)) {
      return false;
    }
    if (this.endTimestamp == null) {
      if (other.endTimestamp != null) {
        return false;
      }
    } else if (!this.endTimestamp.equals(other.endTimestamp)) {
      return false;
    }

    if (this.carRentalInfoId == null) {
      if (other.carRentalInfoId != null) {
        return false;
      }
    } else if (!this.carRentalInfoId.equals(other.carRentalInfoId)) {
      return false;
    }

    if (this.carRentalServiceId == null) {
      if (other.carRentalServiceId != null) {
        return false;
      }
    } else if (!this.carRentalServiceId.equals(other.carRentalServiceId)) {
      return false;
    }
    return true;
  }
}
