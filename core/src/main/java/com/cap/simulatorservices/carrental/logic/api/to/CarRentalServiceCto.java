package com.cap.simulatorservices.carrental.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of CarRentalService
 */
public class CarRentalServiceCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private CarRentalServiceEto carRentalService;

  private CarRentalInfoEto carRentalInfo;

  private RentalCarStatusEto carStatus;

  public CarRentalServiceEto getCarRentalService() {

    return carRentalService;
  }

  public void setCarRentalService(CarRentalServiceEto carRentalService) {

    this.carRentalService = carRentalService;
  }

  public CarRentalInfoEto getCarRentalInfo() {

    return carRentalInfo;
  }

  public void setCarRentalInfo(CarRentalInfoEto carRentalInfo) {

    this.carRentalInfo = carRentalInfo;
  }

  public RentalCarStatusEto getCarStatus() {

    return carStatus;
  }

  public void setCarStatus(RentalCarStatusEto carStatus) {

    this.carStatus = carStatus;
  }

}
