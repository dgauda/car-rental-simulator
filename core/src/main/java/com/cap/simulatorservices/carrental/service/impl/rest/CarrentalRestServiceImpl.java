package com.cap.simulatorservices.carrental.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import com.cap.simulatorservices.carrental.logic.api.Carrental;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusCto;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusEto;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusSearchCriteriaTo;
import com.cap.simulatorservices.carrental.service.api.rest.CarrentalRestService;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Carrental}.
 */
@Named("CarrentalRestService")
public class CarrentalRestServiceImpl implements CarrentalRestService {

  @Inject
  private Carrental carrental;

  @Override
  public CarRentalInfoEto getCarRentalInfo(long id) {

    return this.carrental.findCarRentalInfo(id);
  }

  @Override
  public CarRentalInfoEto saveCarRentalInfo(CarRentalInfoEto carrentalinfo) {

    return this.carrental.saveCarRentalInfo(carrentalinfo);
  }

  @Override
  public void deleteCarRentalInfo(long id) {

    this.carrental.deleteCarRentalInfo(id);
  }

  @Override
  public PaginatedListTo<CarRentalInfoEto> findCarRentalInfosByPost(CarRentalInfoSearchCriteriaTo searchCriteriaTo) {

    return this.carrental.findCarRentalInfoEtos(searchCriteriaTo);
  }

  @Override
  public CarRentalInfoCto getCarRentalInfoCto(long id) {

    return this.carrental.findCarRentalInfoCto(id);
  }

  @Override
  public PaginatedListTo<CarRentalInfoCto> findCarRentalInfoCtosByPost(CarRentalInfoSearchCriteriaTo searchCriteriaTo) {

    return this.carrental.findCarRentalInfoCtos(searchCriteriaTo);
  }

  @Override
  public CarRentalServiceEto getCarRentalService(long id) {

    return this.carrental.findCarRentalService(id);
  }

  @Override
  public CarRentalServiceEto saveCarRentalService(CarRentalServiceEto carrentalservice) {

    return this.carrental.saveCarRentalService(carrentalservice);
  }

  @Override
  public void deleteCarRentalService(long id) {

    this.carrental.deleteCarRentalService(id);
  }

  @Override
  public PaginatedListTo<CarRentalServiceEto> findCarRentalServicesByPost(
      CarRentalServiceSearchCriteriaTo searchCriteriaTo) {

    return this.carrental.findCarRentalServiceEtos(searchCriteriaTo);
  }

  @Override
  public CarRentalServiceCto getCarRentalServiceCto(long id) {

    return this.carrental.findCarRentalServiceCto(id);
  }

  @Override
  public PaginatedListTo<CarRentalServiceCto> findCarRentalServiceCtosByPost(
      CarRentalServiceSearchCriteriaTo searchCriteriaTo) {

    return this.carrental.findCarRentalServiceCtos(searchCriteriaTo);
  }

  @Override
  public CarReservationEto getCarReservation(long id) {

    return this.carrental.findCarReservation(id);
  }

  @Override
  public CarReservationEto saveCarReservation(CarReservationEto carreservation) {

    return this.carrental.saveCarReservation(carreservation);
  }

  @Override
  public void deleteCarReservation(long id) {

    this.carrental.deleteCarReservation(id);
  }

  @Override
  public PaginatedListTo<CarReservationEto> findCarReservationsByPost(CarReservationSearchCriteriaTo searchCriteriaTo) {

    return this.carrental.findCarReservationEtos(searchCriteriaTo);
  }

  @Override
  public CarReservationCto getCarReservationCto(long id) {

    return this.carrental.findCarReservationCto(id);
  }

  @Override
  public PaginatedListTo<CarReservationCto> findCarReservationCtosByPost(
      CarReservationSearchCriteriaTo searchCriteriaTo) {

    return this.carrental.findCarReservationCtos(searchCriteriaTo);
  }

  @Override
  public RentalCarStatusEto getRentalCarStatus(long id) {

    return this.carrental.findRentalCarStatus(id);
  }

  @Override
  public RentalCarStatusEto saveRentalCarStatus(RentalCarStatusEto rentalcarstatus) {

    return this.carrental.saveRentalCarStatus(rentalcarstatus);
  }

  @Override
  public void deleteRentalCarStatus(long id) {

    this.carrental.deleteRentalCarStatus(id);
  }

  @Override
  public PaginatedListTo<RentalCarStatusEto> findRentalCarStatussByPost(
      RentalCarStatusSearchCriteriaTo searchCriteriaTo) {

    return this.carrental.findRentalCarStatusEtos(searchCriteriaTo);
  }

  @Override
  public RentalCarStatusCto getRentalCarStatusCto(long id) {

    return this.carrental.findRentalCarStatusCto(id);
  }

  @Override
  public PaginatedListTo<RentalCarStatusCto> findRentalCarStatusCtosByPost(
      RentalCarStatusSearchCriteriaTo searchCriteriaTo) {

    return this.carrental.findRentalCarStatusCtos(searchCriteriaTo);
  }

}
