package com.cap.simulatorservices.carrental.dataaccess.api;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.cap.simulatorservices.carrental.common.api.RentalCarStatus;
import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author naveengu
 */
@Entity
@Table(name = "RentalCarStatus")
public class RentalCarStatusEntity extends ApplicationPersistenceEntity implements RentalCarStatus {

  private String name;

  private String desc;

  private static final long serialVersionUID = 1L;

  /**
   * @return name
   */
  public String getName() {

    return this.name;
  }

  /**
   * @param name new value of {@link #getname}.
   */
  public void setName(String name) {

    this.name = name;
  }

  /**
   * @return desc
   */
  public String getDesc() {

    return this.desc;
  }

  /**
   * @param desc new value of {@link #getdesc}.
   */
  public void setDesc(String desc) {

    this.desc = desc;
  }

}
