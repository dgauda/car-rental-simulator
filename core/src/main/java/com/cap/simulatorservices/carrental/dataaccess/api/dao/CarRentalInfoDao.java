package com.cap.simulatorservices.carrental.dataaccess.api.dao;

import com.cap.simulatorservices.carrental.dataaccess.api.CarRentalInfoEntity;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.api.dao.ApplicationDao;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for CarRentalInfo entities
 */
public interface CarRentalInfoDao extends ApplicationDao<CarRentalInfoEntity> {

  /**
   * Finds the {@link CarRentalInfoEntity carrentalinfos} matching the given {@link CarRentalInfoSearchCriteriaTo}.
   *
   * @param criteria is the {@link CarRentalInfoSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link CarRentalInfoEntity} objects.
   */
  PaginatedListTo<CarRentalInfoEntity> findCarRentalInfos(CarRentalInfoSearchCriteriaTo criteria);
}
