package com.cap.simulatorservices.carrental.logic.api.to;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link com.cap.simulatorservices.carrental.common.api.CarRentalInfo}s.
 *
 */
public class CarRentalInfoSearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String address;

  private Long zipcode;

  private Long totalNoOfCars;

  private Long availableNoOfCars;

  private String operatingcompany;

  /**
   * The constructor.
   */
  public CarRentalInfoSearchCriteriaTo() {

    super();
  }

  public String getAddress() {

    return address;
  }

  public void setAddress(String address) {

    this.address = address;
  }

  public Long getZipcode() {

    return zipcode;
  }

  public void setZipcode(Long zipcode) {

    this.zipcode = zipcode;
  }

  public Long getTotalNoOfCars() {

    return totalNoOfCars;
  }

  public void setTotalNoOfCars(Long totalNoOfCars) {

    this.totalNoOfCars = totalNoOfCars;
  }

  public Long getAvailableNoOfCars() {

    return availableNoOfCars;
  }

  public void setAvailableNoOfCars(Long availableNoOfCars) {

    this.availableNoOfCars = availableNoOfCars;
  }

  public String getOperatingcompany() {

    return operatingcompany;
  }

  public void setOperatingcompany(String operatingcompany) {

    this.operatingcompany = operatingcompany;
  }

}
