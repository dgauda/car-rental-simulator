package com.cap.simulatorservices.carrental.common.api;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface CarRentalService extends ApplicationEntity {

  public Long getCarRentalInfoId();

  public void setCarRentalInfoId(Long carRentalInfoId);

  public String getCarDetail();

  public void setCarDetail(String carDetail);

  public Long getCarStatusId();

  public void setCarStatusId(Long carStatusId);

}
