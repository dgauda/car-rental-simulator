package com.cap.simulatorservices.carrental.logic.api;

import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusCto;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusEto;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Interface for Carrental component.
 */
public interface Carrental {

  /**
   * Returns a CarRentalInfo by its id 'id'.
   *
   * @param id The id 'id' of the CarRentalInfo.
   * @return The {@link CarRentalInfoEto} with id 'id'
   */
  CarRentalInfoEto findCarRentalInfo(Long id);

  /**
   * Returns a paginated list of CarRentalInfos matching the search criteria.
   *
   * @param criteria the {@link CarRentalInfoSearchCriteriaTo}.
   * @return the {@link List} of matching {@link CarRentalInfoEto}s.
   */
  PaginatedListTo<CarRentalInfoEto> findCarRentalInfoEtos(CarRentalInfoSearchCriteriaTo criteria);

  /**
   * Deletes a carRentalInfo from the database by its id 'carRentalInfoId'.
   *
   * @param carRentalInfoId Id of the carRentalInfo to delete
   * @return boolean <code>true</code> if the carRentalInfo can be deleted, <code>false</code> otherwise
   */
  boolean deleteCarRentalInfo(Long carRentalInfoId);

  /**
   * Saves a carRentalInfo and store it in the database.
   *
   * @param carRentalInfo the {@link CarRentalInfoEto} to create.
   * @return the new {@link CarRentalInfoEto} that has been saved with ID and version.
   */
  CarRentalInfoEto saveCarRentalInfo(CarRentalInfoEto carRentalInfo);

  /**
   * Returns a composite CarRentalInfo by its id 'id'.
   *
   * @param id The id 'id' of the CarRentalInfo.
   * @return The {@link CarRentalInfoCto} with id 'id'
   */
  CarRentalInfoCto findCarRentalInfoCto(Long id);

  /**
   * Returns a paginated list of composite CarRentalInfos matching the search criteria.
   *
   * @param criteria the {@link CarRentalInfoSearchCriteriaTo}.
   * @return the {@link List} of matching {@link CarRentalInfoCto}s.
   */
  PaginatedListTo<CarRentalInfoCto> findCarRentalInfoCtos(CarRentalInfoSearchCriteriaTo criteria);

  /**
   * Returns a CarRentalService by its id 'id'.
   *
   * @param id The id 'id' of the CarRentalService.
   * @return The {@link CarRentalServiceEto} with id 'id'
   */
  CarRentalServiceEto findCarRentalService(Long id);

  /**
   * Returns a paginated list of CarRentalServices matching the search criteria.
   *
   * @param criteria the {@link CarRentalServiceSearchCriteriaTo}.
   * @return the {@link List} of matching {@link CarRentalServiceEto}s.
   */
  PaginatedListTo<CarRentalServiceEto> findCarRentalServiceEtos(CarRentalServiceSearchCriteriaTo criteria);

  /**
   * Deletes a carRentalService from the database by its id 'carRentalServiceId'.
   *
   * @param carRentalServiceId Id of the carRentalService to delete
   * @return boolean <code>true</code> if the carRentalService can be deleted, <code>false</code> otherwise
   */
  boolean deleteCarRentalService(Long carRentalServiceId);

  /**
   * Saves a carRentalService and store it in the database.
   *
   * @param carRentalService the {@link CarRentalServiceEto} to create.
   * @return the new {@link CarRentalServiceEto} that has been saved with ID and version.
   */
  CarRentalServiceEto saveCarRentalService(CarRentalServiceEto carRentalService);

  /**
   * Returns a composite CarRentalService by its id 'id'.
   *
   * @param id The id 'id' of the CarRentalService.
   * @return The {@link CarRentalServiceCto} with id 'id'
   */
  CarRentalServiceCto findCarRentalServiceCto(Long id);

  /**
   * Returns a paginated list of composite CarRentalServices matching the search criteria.
   *
   * @param criteria the {@link CarRentalServiceSearchCriteriaTo}.
   * @return the {@link List} of matching {@link CarRentalServiceCto}s.
   */
  PaginatedListTo<CarRentalServiceCto> findCarRentalServiceCtos(CarRentalServiceSearchCriteriaTo criteria);

  /**
   * Returns a CarReservation by its id 'id'.
   *
   * @param id The id 'id' of the CarReservation.
   * @return The {@link CarReservationEto} with id 'id'
   */
  CarReservationEto findCarReservation(Long id);

  /**
   * Returns a paginated list of CarReservations matching the search criteria.
   *
   * @param criteria the {@link CarReservationSearchCriteriaTo}.
   * @return the {@link List} of matching {@link CarReservationEto}s.
   */
  PaginatedListTo<CarReservationEto> findCarReservationEtos(CarReservationSearchCriteriaTo criteria);

  /**
   * Deletes a carReservation from the database by its id 'carReservationId'.
   *
   * @param carReservationId Id of the carReservation to delete
   * @return boolean <code>true</code> if the carReservation can be deleted, <code>false</code> otherwise
   */
  boolean deleteCarReservation(Long carReservationId);

  /**
   * Saves a carReservation and store it in the database.
   *
   * @param carReservation the {@link CarReservationEto} to create.
   * @return the new {@link CarReservationEto} that has been saved with ID and version.
   */
  CarReservationEto saveCarReservation(CarReservationEto carReservation);

  /**
   * Returns a composite CarReservation by its id 'id'.
   *
   * @param id The id 'id' of the CarReservation.
   * @return The {@link CarReservationCto} with id 'id'
   */
  CarReservationCto findCarReservationCto(Long id);

  /**
   * Returns a paginated list of composite CarReservations matching the search criteria.
   *
   * @param criteria the {@link CarReservationSearchCriteriaTo}.
   * @return the {@link List} of matching {@link CarReservationCto}s.
   */
  PaginatedListTo<CarReservationCto> findCarReservationCtos(CarReservationSearchCriteriaTo criteria);

  /**
   * Returns a RentalCarStatus by its id 'id'.
   *
   * @param id The id 'id' of the RentalCarStatus.
   * @return The {@link RentalCarStatusEto} with id 'id'
   */
  RentalCarStatusEto findRentalCarStatus(Long id);

  /**
   * Returns a paginated list of RentalCarStatuss matching the search criteria.
   *
   * @param criteria the {@link RentalCarStatusSearchCriteriaTo}.
   * @return the {@link List} of matching {@link RentalCarStatusEto}s.
   */
  PaginatedListTo<RentalCarStatusEto> findRentalCarStatusEtos(RentalCarStatusSearchCriteriaTo criteria);

  /**
   * Deletes a rentalCarStatus from the database by its id 'rentalCarStatusId'.
   *
   * @param rentalCarStatusId Id of the rentalCarStatus to delete
   * @return boolean <code>true</code> if the rentalCarStatus can be deleted, <code>false</code> otherwise
   */
  boolean deleteRentalCarStatus(Long rentalCarStatusId);

  /**
   * Saves a rentalCarStatus and store it in the database.
   *
   * @param rentalCarStatus the {@link RentalCarStatusEto} to create.
   * @return the new {@link RentalCarStatusEto} that has been saved with ID and version.
   */
  RentalCarStatusEto saveRentalCarStatus(RentalCarStatusEto rentalCarStatus);

  /**
   * Returns a composite RentalCarStatus by its id 'id'.
   *
   * @param id The id 'id' of the RentalCarStatus.
   * @return The {@link RentalCarStatusCto} with id 'id'
   */
  RentalCarStatusCto findRentalCarStatusCto(Long id);

  /**
   * Returns a paginated list of composite RentalCarStatuss matching the search criteria.
   *
   * @param criteria the {@link RentalCarStatusSearchCriteriaTo}.
   * @return the {@link List} of matching {@link RentalCarStatusCto}s.
   */
  PaginatedListTo<RentalCarStatusCto> findRentalCarStatusCtos(RentalCarStatusSearchCriteriaTo criteria);

}
