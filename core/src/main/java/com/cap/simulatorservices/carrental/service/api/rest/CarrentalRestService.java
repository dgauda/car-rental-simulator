package com.cap.simulatorservices.carrental.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cap.simulatorservices.carrental.logic.api.Carrental;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusCto;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusEto;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Carrental}.
 */
@Path("/carrental/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface CarrentalRestService {

  /**
   * Delegates to {@link Carrental#findCarRentalInfo}.
   *
   * @param id the ID of the {@link CarRentalInfoEto}
   * @return the {@link CarRentalInfoEto}
   */
  @GET
  @Path("/carrentalinfo/{id}/")
  public CarRentalInfoEto getCarRentalInfo(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#saveCarRentalInfo}.
   *
   * @param carrentalinfo the {@link CarRentalInfoEto} to be saved
   * @return the recently created {@link CarRentalInfoEto}
   */
  @POST
  @Path("/carrentalinfo/")
  public CarRentalInfoEto saveCarRentalInfo(CarRentalInfoEto carrentalinfo);

  /**
   * Delegates to {@link Carrental#deleteCarRentalInfo}.
   *
   * @param id ID of the {@link CarRentalInfoEto} to be deleted
   */
  @DELETE
  @Path("/carrentalinfo/{id}/")
  public void deleteCarRentalInfo(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#findCarRentalInfoEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding carrentalinfos.
   * @return the {@link PaginatedListTo list} of matching {@link CarRentalInfoEto}s.
   */
  @Path("/carrentalinfo/search")
  @POST
  public PaginatedListTo<CarRentalInfoEto> findCarRentalInfosByPost(CarRentalInfoSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Carrental#findCarRentalInfoCto}.
   *
   * @param id the ID of the {@link CarRentalInfoCto}
   * @return the {@link CarRentalInfoCto}
   */
  @GET
  @Path("/carrentalinfo/cto/{id}/")
  public CarRentalInfoCto getCarRentalInfoCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#findCarRentalInfoCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding carrentalinfos.
   * @return the {@link PaginatedListTo list} of matching {@link CarRentalInfoCto}s.
   */
  @Path("/carrentalinfo/cto/search")
  @POST
  public PaginatedListTo<CarRentalInfoCto> findCarRentalInfoCtosByPost(CarRentalInfoSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Carrental#findCarRentalService}.
   *
   * @param id the ID of the {@link CarRentalServiceEto}
   * @return the {@link CarRentalServiceEto}
   */
  @GET
  @Path("/carrentalservice/{id}/")
  public CarRentalServiceEto getCarRentalService(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#saveCarRentalService}.
   *
   * @param carrentalservice the {@link CarRentalServiceEto} to be saved
   * @return the recently created {@link CarRentalServiceEto}
   */
  @POST
  @Path("/carrentalservice/")
  public CarRentalServiceEto saveCarRentalService(CarRentalServiceEto carrentalservice);

  /**
   * Delegates to {@link Carrental#deleteCarRentalService}.
   *
   * @param id ID of the {@link CarRentalServiceEto} to be deleted
   */
  @DELETE
  @Path("/carrentalservice/{id}/")
  public void deleteCarRentalService(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#findCarRentalServiceEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding carrentalservices.
   * @return the {@link PaginatedListTo list} of matching {@link CarRentalServiceEto}s.
   */
  @Path("/carrentalservice/search")
  @POST
  public PaginatedListTo<CarRentalServiceEto> findCarRentalServicesByPost(
      CarRentalServiceSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Carrental#findCarRentalServiceCto}.
   *
   * @param id the ID of the {@link CarRentalServiceCto}
   * @return the {@link CarRentalServiceCto}
   */
  @GET
  @Path("/carrentalservice/cto/{id}/")
  public CarRentalServiceCto getCarRentalServiceCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#findCarRentalServiceCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding carrentalservices.
   * @return the {@link PaginatedListTo list} of matching {@link CarRentalServiceCto}s.
   */
  @Path("/carrentalservice/cto/search")
  @POST
  public PaginatedListTo<CarRentalServiceCto> findCarRentalServiceCtosByPost(
      CarRentalServiceSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Carrental#findCarReservation}.
   *
   * @param id the ID of the {@link CarReservationEto}
   * @return the {@link CarReservationEto}
   */
  @GET
  @Path("/carreservation/{id}/")
  public CarReservationEto getCarReservation(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#saveCarReservation}.
   *
   * @param carreservation the {@link CarReservationEto} to be saved
   * @return the recently created {@link CarReservationEto}
   */
  @POST
  @Path("/carreservation/")
  public CarReservationEto saveCarReservation(CarReservationEto carreservation);

  /**
   * Delegates to {@link Carrental#deleteCarReservation}.
   *
   * @param id ID of the {@link CarReservationEto} to be deleted
   */
  @DELETE
  @Path("/carreservation/{id}/")
  public void deleteCarReservation(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#findCarReservationEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding carreservations.
   * @return the {@link PaginatedListTo list} of matching {@link CarReservationEto}s.
   */
  @Path("/carreservation/search")
  @POST
  public PaginatedListTo<CarReservationEto> findCarReservationsByPost(CarReservationSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Carrental#findCarReservationCto}.
   *
   * @param id the ID of the {@link CarReservationCto}
   * @return the {@link CarReservationCto}
   */
  @GET
  @Path("/carreservation/cto/{id}/")
  public CarReservationCto getCarReservationCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#findCarReservationCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding carreservations.
   * @return the {@link PaginatedListTo list} of matching {@link CarReservationCto}s.
   */
  @Path("/carreservation/cto/search")
  @POST
  public PaginatedListTo<CarReservationCto> findCarReservationCtosByPost(
      CarReservationSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Carrental#findRentalCarStatus}.
   *
   * @param id the ID of the {@link RentalCarStatusEto}
   * @return the {@link RentalCarStatusEto}
   */
  @GET
  @Path("/rentalcarstatus/{id}/")
  public RentalCarStatusEto getRentalCarStatus(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#saveRentalCarStatus}.
   *
   * @param rentalcarstatus the {@link RentalCarStatusEto} to be saved
   * @return the recently created {@link RentalCarStatusEto}
   */
  @POST
  @Path("/rentalcarstatus/")
  public RentalCarStatusEto saveRentalCarStatus(RentalCarStatusEto rentalcarstatus);

  /**
   * Delegates to {@link Carrental#deleteRentalCarStatus}.
   *
   * @param id ID of the {@link RentalCarStatusEto} to be deleted
   */
  @DELETE
  @Path("/rentalcarstatus/{id}/")
  public void deleteRentalCarStatus(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#findRentalCarStatusEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding rentalcarstatuss.
   * @return the {@link PaginatedListTo list} of matching {@link RentalCarStatusEto}s.
   */
  @Path("/rentalcarstatus/search")
  @POST
  public PaginatedListTo<RentalCarStatusEto> findRentalCarStatussByPost(
      RentalCarStatusSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Carrental#findRentalCarStatusCto}.
   *
   * @param id the ID of the {@link RentalCarStatusCto}
   * @return the {@link RentalCarStatusCto}
   */
  @GET
  @Path("/rentalcarstatus/cto/{id}/")
  public RentalCarStatusCto getRentalCarStatusCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Carrental#findRentalCarStatusCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding rentalcarstatuss.
   * @return the {@link PaginatedListTo list} of matching {@link RentalCarStatusCto}s.
   */
  @Path("/rentalcarstatus/cto/search")
  @POST
  public PaginatedListTo<RentalCarStatusCto> findRentalCarStatusCtosByPost(
      RentalCarStatusSearchCriteriaTo searchCriteriaTo);

}
