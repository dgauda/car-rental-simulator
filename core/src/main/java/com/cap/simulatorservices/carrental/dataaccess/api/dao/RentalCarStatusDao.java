package com.cap.simulatorservices.carrental.dataaccess.api.dao;

import com.cap.simulatorservices.carrental.dataaccess.api.RentalCarStatusEntity;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.api.dao.ApplicationDao;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for RentalCarStatus entities
 */
public interface RentalCarStatusDao extends ApplicationDao<RentalCarStatusEntity> {

  /**
   * Finds the {@link RentalCarStatusEntity rentalcarstatuss} matching the given
   * {@link RentalCarStatusSearchCriteriaTo}.
   *
   * @param criteria is the {@link RentalCarStatusSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link RentalCarStatusEntity} objects.
   */
  PaginatedListTo<RentalCarStatusEntity> findRentalCarStatuss(RentalCarStatusSearchCriteriaTo criteria);
}
