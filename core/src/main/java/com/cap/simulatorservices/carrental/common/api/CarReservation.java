package com.cap.simulatorservices.carrental.common.api;

import java.sql.Timestamp;

import com.cap.simulatorservices.general.common.api.ApplicationEntity;

public interface CarReservation extends ApplicationEntity {

  public Long getItineraryId();

  public void setItineraryId(Long itineraryId);

  public Timestamp getStartTimestamp();

  public void setStartTimestamp(Timestamp startTimestamp);

  public Timestamp getEndTimestamp();

  public void setEndTimestamp(Timestamp endTimestamp);

  public Long getCarRentalInfoId();

  public void setCarRentalInfoId(Long carRentalInfoId);

  public Long getCarRentalServiceId();

  public void setCarRentalServiceId(Long carRentalServiceId);

}
