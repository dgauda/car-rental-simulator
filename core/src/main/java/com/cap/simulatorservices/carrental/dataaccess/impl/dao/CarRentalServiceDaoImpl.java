package com.cap.simulatorservices.carrental.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.carrental.dataaccess.api.CarRentalServiceEntity;
import com.cap.simulatorservices.carrental.dataaccess.api.dao.CarRentalServiceDao;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link CarRentalServiceDao}.
 */
@Named
public class CarRentalServiceDaoImpl extends ApplicationDaoImpl<CarRentalServiceEntity> implements CarRentalServiceDao {

  /**
   * The constructor.
   */
  public CarRentalServiceDaoImpl() {

    super();
  }

  @Override
  public Class<CarRentalServiceEntity> getEntityClass() {

    return CarRentalServiceEntity.class;
  }

  @Override
  public PaginatedListTo<CarRentalServiceEntity> findCarRentalServices(CarRentalServiceSearchCriteriaTo criteria) {

    CarRentalServiceEntity carrentalservice = Alias.alias(CarRentalServiceEntity.class);
    EntityPathBase<CarRentalServiceEntity> alias = Alias.$(carrentalservice);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    Long carRentalInfo = criteria.getCarRentalInfoId();
    if (carRentalInfo != null) {
      if (carrentalservice.getCarRentalInfo() != null) {
        query.where(Alias.$(carrentalservice.getCarRentalInfo().getId()).eq(carRentalInfo));
      }
    }
    String carDetail = criteria.getCarDetail();
    if (carDetail != null) {
      query.where(Alias.$(carrentalservice.getCarDetail()).eq(carDetail));
    }
    Long carStatus = criteria.getCarStatusId();
    if (carStatus != null) {
      if (carrentalservice.getCarStatus() != null) {
        query.where(Alias.$(carrentalservice.getCarStatus().getId()).eq(carStatus));
      }
    }
    addOrderBy(query, alias, carrentalservice, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<CarRentalServiceEntity> alias,
      CarRentalServiceEntity carrentalservice, List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "carRentalInfo":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carrentalservice.getCarRentalInfo().getId()).asc());
            } else {
              query.orderBy(Alias.$(carrentalservice.getCarRentalInfo().getId()).desc());
            }
            break;
          case "carDetail":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carrentalservice.getCarDetail()).asc());
            } else {
              query.orderBy(Alias.$(carrentalservice.getCarDetail()).desc());
            }
            break;
          case "carStatus":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carrentalservice.getCarStatus().getId()).asc());
            } else {
              query.orderBy(Alias.$(carrentalservice.getCarStatus().getId()).desc());
            }
            break;
        }
      }
    }
  }

}