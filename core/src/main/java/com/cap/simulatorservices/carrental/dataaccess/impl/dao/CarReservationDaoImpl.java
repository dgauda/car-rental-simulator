package com.cap.simulatorservices.carrental.dataaccess.impl.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.carrental.dataaccess.api.CarReservationEntity;
import com.cap.simulatorservices.carrental.dataaccess.api.dao.CarReservationDao;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link CarReservationDao}.
 */
@Named
public class CarReservationDaoImpl extends ApplicationDaoImpl<CarReservationEntity> implements CarReservationDao {

  /**
   * The constructor.
   */
  public CarReservationDaoImpl() {

    super();
  }

  @Override
  public Class<CarReservationEntity> getEntityClass() {

    return CarReservationEntity.class;
  }

  @Override
  public PaginatedListTo<CarReservationEntity> findCarReservations(CarReservationSearchCriteriaTo criteria) {

    CarReservationEntity carreservation = Alias.alias(CarReservationEntity.class);
    EntityPathBase<CarReservationEntity> alias = Alias.$(carreservation);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    Long itineraryId = criteria.getItineraryId();
    if (itineraryId != null) {
      query.where(Alias.$(carreservation.getItineraryId()).eq(itineraryId));
    }
    Timestamp startTimestamp = criteria.getStartTimestamp();
    if (startTimestamp != null) {
      query.where(Alias.$(carreservation.getStartTimestamp()).eq(startTimestamp));
    }
    Timestamp endTimestamp = criteria.getEndTimestamp();
    if (endTimestamp != null) {
      query.where(Alias.$(carreservation.getEndTimestamp()).eq(endTimestamp));
    }
    Long carRentalInfo = criteria.getCarRentalInfoId();
    if (carRentalInfo != null) {
      if (carreservation.getCarRentalInfo() != null) {
        query.where(Alias.$(carreservation.getCarRentalInfo().getId()).eq(carRentalInfo));
      }
    }
    Long carRentalService = criteria.getCarRentalServiceId();
    if (carRentalService != null) {
      if (carreservation.getCarRentalService() != null) {
        query.where(Alias.$(carreservation.getCarRentalService().getId()).eq(carRentalService));
      }
    }
    addOrderBy(query, alias, carreservation, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<CarReservationEntity> alias,
      CarReservationEntity carreservation, List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "itineraryId":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carreservation.getItineraryId()).asc());
            } else {
              query.orderBy(Alias.$(carreservation.getItineraryId()).desc());
            }
            break;
          case "startTimestamp":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carreservation.getStartTimestamp()).asc());
            } else {
              query.orderBy(Alias.$(carreservation.getStartTimestamp()).desc());
            }
            break;
          case "endTimestamp":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carreservation.getEndTimestamp()).asc());
            } else {
              query.orderBy(Alias.$(carreservation.getEndTimestamp()).desc());
            }
            break;
          case "carRentalInfo":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carreservation.getCarRentalInfo().getId()).asc());
            } else {
              query.orderBy(Alias.$(carreservation.getCarRentalInfo().getId()).desc());
            }
            break;
          case "carRentalService":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(carreservation.getCarRentalService().getId()).asc());
            } else {
              query.orderBy(Alias.$(carreservation.getCarRentalService().getId()).desc());
            }
            break;
        }
      }
    }
  }

}