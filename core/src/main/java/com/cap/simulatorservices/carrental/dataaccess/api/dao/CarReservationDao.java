package com.cap.simulatorservices.carrental.dataaccess.api.dao;

import com.cap.simulatorservices.carrental.dataaccess.api.CarReservationEntity;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationSearchCriteriaTo;
import com.cap.simulatorservices.general.dataaccess.api.dao.ApplicationDao;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for CarReservation entities
 */
public interface CarReservationDao extends ApplicationDao<CarReservationEntity> {

  /**
   * Finds the {@link CarReservationEntity carreservations} matching the given {@link CarReservationSearchCriteriaTo}.
   *
   * @param criteria is the {@link CarReservationSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link CarReservationEntity} objects.
   */
  PaginatedListTo<CarReservationEntity> findCarReservations(CarReservationSearchCriteriaTo criteria);
}
