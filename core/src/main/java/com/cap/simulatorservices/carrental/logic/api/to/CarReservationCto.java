package com.cap.simulatorservices.carrental.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of CarReservation
 */
public class CarReservationCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private CarReservationEto carReservation;

  private CarRentalInfoEto carRentalInfo;

  private CarRentalServiceEto carRentalService;

  public CarReservationEto getCarReservation() {

    return carReservation;
  }

  public void setCarReservation(CarReservationEto carReservation) {

    this.carReservation = carReservation;
  }

  public CarRentalInfoEto getCarRentalInfo() {

    return carRentalInfo;
  }

  public void setCarRentalInfo(CarRentalInfoEto carRentalInfo) {

    this.carRentalInfo = carRentalInfo;
  }

  public CarRentalServiceEto getCarRentalService() {

    return carRentalService;
  }

  public void setCarRentalService(CarRentalServiceEto carRentalService) {

    this.carRentalService = carRentalService;
  }

}
