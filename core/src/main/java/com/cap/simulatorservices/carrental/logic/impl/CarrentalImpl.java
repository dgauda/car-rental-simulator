package com.cap.simulatorservices.carrental.logic.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.cap.simulatorservices.carrental.dataaccess.api.CarRentalInfoEntity;
import com.cap.simulatorservices.carrental.dataaccess.api.CarRentalServiceEntity;
import com.cap.simulatorservices.carrental.dataaccess.api.CarReservationEntity;
import com.cap.simulatorservices.carrental.dataaccess.api.RentalCarStatusEntity;
import com.cap.simulatorservices.carrental.dataaccess.api.dao.CarRentalInfoDao;
import com.cap.simulatorservices.carrental.dataaccess.api.dao.CarRentalServiceDao;
import com.cap.simulatorservices.carrental.dataaccess.api.dao.CarReservationDao;
import com.cap.simulatorservices.carrental.dataaccess.api.dao.RentalCarStatusDao;
import com.cap.simulatorservices.carrental.logic.api.Carrental;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalInfoSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarRentalServiceSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationCto;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationEto;
import com.cap.simulatorservices.carrental.logic.api.to.CarReservationSearchCriteriaTo;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusCto;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusEto;
import com.cap.simulatorservices.carrental.logic.api.to.RentalCarStatusSearchCriteriaTo;
import com.cap.simulatorservices.general.logic.base.AbstractComponentFacade;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;
import io.oasp.module.jpa.common.api.to.PaginationResultTo;

/**
 * Implementation of component interface of carrental
 */
@Named
@Transactional
public class CarrentalImpl extends AbstractComponentFacade implements Carrental {

  /**
   * Logger instance.
   */
  private static final Logger LOG = LoggerFactory.getLogger(CarrentalImpl.class);

  /**
   * @see #getCarRentalInfoDao()
   */
  @Inject
  private CarRentalInfoDao carRentalInfoDao;

  /**
   * @see #getCarRentalServiceDao()
   */
  @Inject
  private CarRentalServiceDao carRentalServiceDao;

  /**
   * @see #getCarReservationDao()
   */
  @Inject
  private CarReservationDao carReservationDao;

  /**
   * @see #getRentalCarStatusDao()
   */
  @Inject
  private RentalCarStatusDao rentalCarStatusDao;

  /**
   * The constructor.
   */
  public CarrentalImpl() {

    super();
  }

  @Override
  public CarRentalInfoEto findCarRentalInfo(Long id) {

    LOG.info("Get CarRentalInfo with id {} from database.", id);
    return getBeanMapper().map(getCarRentalInfoDao().findOne(id), CarRentalInfoEto.class);
  }

  @Override
  public PaginatedListTo<CarRentalInfoEto> findCarRentalInfoEtos(CarRentalInfoSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<CarRentalInfoEntity> carrentalinfos = getCarRentalInfoDao().findCarRentalInfos(criteria);
    return mapPaginatedEntityList(carrentalinfos, CarRentalInfoEto.class);
  }

  @Override
  public boolean deleteCarRentalInfo(Long carRentalInfoId) {

    CarRentalInfoEntity carRentalInfo = getCarRentalInfoDao().find(carRentalInfoId);
    getCarRentalInfoDao().delete(carRentalInfo);
    LOG.debug("The carRentalInfo with id '{}' has been deleted.", carRentalInfoId);
    return true;
  }

  @Override
  public CarRentalInfoEto saveCarRentalInfo(CarRentalInfoEto carRentalInfo) {

    Objects.requireNonNull(carRentalInfo, "carRentalInfo");
    CarRentalInfoEntity carRentalInfoEntity = getBeanMapper().map(carRentalInfo, CarRentalInfoEntity.class);

    // initialize, validate carRentalInfoEntity here if necessary
    CarRentalInfoEntity resultEntity = getCarRentalInfoDao().save(carRentalInfoEntity);
    LOG.debug("CarRentalInfo with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, CarRentalInfoEto.class);
  }

  /**
   * Returns the field 'carRentalInfoDao'.
   *
   * @return the {@link CarRentalInfoDao} instance.
   */
  public CarRentalInfoDao getCarRentalInfoDao() {

    return this.carRentalInfoDao;
  }

  @Override
  public CarRentalInfoCto findCarRentalInfoCto(Long id) {

    LOG.debug("Get CarRentalInfoCto with id {} from database.", id);
    CarRentalInfoEntity entity = getCarRentalInfoDao().findOne(id);
    CarRentalInfoCto cto = new CarRentalInfoCto();
    cto.setCarRentalInfo(getBeanMapper().map(entity, CarRentalInfoEto.class));
    cto.setCarRentalService(getBeanMapper().mapSet(entity.getCarRentalService(), CarRentalServiceEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<CarRentalInfoCto> findCarRentalInfoCtos(CarRentalInfoSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<CarRentalInfoEntity> carrentalinfos = getCarRentalInfoDao().findCarRentalInfos(criteria);
    List<CarRentalInfoCto> ctos = new ArrayList<>();
    for (CarRentalInfoEntity entity : carrentalinfos.getResult()) {
      CarRentalInfoCto cto = new CarRentalInfoCto();
      cto.setCarRentalInfo(getBeanMapper().map(entity, CarRentalInfoEto.class));
      cto.setCarRentalService(getBeanMapper().mapSet(entity.getCarRentalService(), CarRentalServiceEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<CarRentalInfoCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

  @Override
  public CarRentalServiceEto findCarRentalService(Long id) {

    LOG.debug("Get CarRentalService with id {} from database.", id);
    return getBeanMapper().map(getCarRentalServiceDao().findOne(id), CarRentalServiceEto.class);
  }

  @Override
  public PaginatedListTo<CarRentalServiceEto> findCarRentalServiceEtos(CarRentalServiceSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<CarRentalServiceEntity> carrentalservices = getCarRentalServiceDao()
        .findCarRentalServices(criteria);
    return mapPaginatedEntityList(carrentalservices, CarRentalServiceEto.class);
  }

  @Override
  public boolean deleteCarRentalService(Long carRentalServiceId) {

    CarRentalServiceEntity carRentalService = getCarRentalServiceDao().find(carRentalServiceId);
    getCarRentalServiceDao().delete(carRentalService);
    LOG.debug("The carRentalService with id '{}' has been deleted.", carRentalServiceId);
    return true;
  }

  @Override
  public CarRentalServiceEto saveCarRentalService(CarRentalServiceEto carRentalService) {

    Objects.requireNonNull(carRentalService, "carRentalService");
    CarRentalServiceEntity carRentalServiceEntity = getBeanMapper().map(carRentalService, CarRentalServiceEntity.class);

    // initialize, validate carRentalServiceEntity here if necessary
    CarRentalServiceEntity resultEntity = getCarRentalServiceDao().save(carRentalServiceEntity);
    LOG.debug("CarRentalService with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, CarRentalServiceEto.class);
  }

  /**
   * Returns the field 'carRentalServiceDao'.
   *
   * @return the {@link CarRentalServiceDao} instance.
   */
  public CarRentalServiceDao getCarRentalServiceDao() {

    return this.carRentalServiceDao;
  }

  @Override
  public CarRentalServiceCto findCarRentalServiceCto(Long id) {

    LOG.debug("Get CarRentalServiceCto with id {} from database.", id);
    CarRentalServiceEntity entity = getCarRentalServiceDao().findOne(id);
    CarRentalServiceCto cto = new CarRentalServiceCto();
    cto.setCarRentalService(getBeanMapper().map(entity, CarRentalServiceEto.class));
    cto.setCarRentalInfo(getBeanMapper().map(entity.getCarRentalInfo(), CarRentalInfoEto.class));
    cto.setCarStatus(getBeanMapper().map(entity.getCarStatus(), RentalCarStatusEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<CarRentalServiceCto> findCarRentalServiceCtos(CarRentalServiceSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<CarRentalServiceEntity> carrentalservices = getCarRentalServiceDao()
        .findCarRentalServices(criteria);
    List<CarRentalServiceCto> ctos = new ArrayList<>();
    for (CarRentalServiceEntity entity : carrentalservices.getResult()) {
      CarRentalServiceCto cto = new CarRentalServiceCto();
      cto.setCarRentalService(getBeanMapper().map(entity, CarRentalServiceEto.class));
      cto.setCarRentalInfo(getBeanMapper().map(entity.getCarRentalInfo(), CarRentalInfoEto.class));
      cto.setCarStatus(getBeanMapper().map(entity.getCarStatus(), RentalCarStatusEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<CarRentalServiceCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

  @Override
  public CarReservationEto findCarReservation(Long id) {

    LOG.debug("Get CarReservation with id {} from database.", id);
    return getBeanMapper().map(getCarReservationDao().findOne(id), CarReservationEto.class);
  }

  @Override
  public PaginatedListTo<CarReservationEto> findCarReservationEtos(CarReservationSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<CarReservationEntity> carreservations = getCarReservationDao().findCarReservations(criteria);
    return mapPaginatedEntityList(carreservations, CarReservationEto.class);
  }

  @Override
  public boolean deleteCarReservation(Long carReservationId) {

    CarReservationEntity carReservation = getCarReservationDao().find(carReservationId);
    getCarReservationDao().delete(carReservation);
    LOG.debug("The carReservation with id '{}' has been deleted.", carReservationId);
    return true;
  }

  @Override
  public CarReservationEto saveCarReservation(CarReservationEto carReservation) {

    Objects.requireNonNull(carReservation, "carReservation");
    CarRentalServiceEto carRentalServiceEto = findCarRentalService(carReservation.getCarRentalServiceId());
    CarRentalInfoEto carRentalInfoEto = findCarRentalInfo(carReservation.getCarRentalInfoId());

    carRentalServiceEto.setCarStatusId(2L);

    carRentalInfoEto.setAvailableNoOfCars(carRentalInfoEto.getAvailableNoOfCars() - 1);

    CarReservationEntity carReservationEntity = getBeanMapper().map(carReservation, CarReservationEntity.class);
    carReservationEntity.setCarRentalInfo(getCarRentalInfoDao().findOne(carRentalInfoEto.getId()));
    carReservationEntity.setCarRentalService(getCarRentalServiceDao().findOne(carRentalServiceEto.getId()));
    carReservationEntity.setItineraryId(carReservation.getItineraryId());

    carRentalServiceEto = saveCarRentalService(carRentalServiceEto);
    carRentalInfoEto = saveCarRentalInfo(carRentalInfoEto);
    CarReservationEntity resultEntity = getCarReservationDao().save(carReservationEntity);

    CarReservationEto resultEto = getBeanMapper().map(resultEntity, CarReservationEto.class);
    LOG.debug("CarReservation with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, CarReservationEto.class);
  }

  /**
   * Returns the field 'carReservationDao'.
   *
   * @return the {@link CarReservationDao} instance.
   */
  public CarReservationDao getCarReservationDao() {

    return this.carReservationDao;
  }

  @Override
  public CarReservationCto findCarReservationCto(Long id) {

    LOG.debug("Get CarReservationCto with id {} from database.", id);
    CarReservationEntity entity = getCarReservationDao().findOne(id);
    CarReservationCto cto = new CarReservationCto();
    cto.setCarReservation(getBeanMapper().map(entity, CarReservationEto.class));
    cto.setCarRentalInfo(getBeanMapper().map(entity.getCarRentalInfo(), CarRentalInfoEto.class));
    cto.setCarRentalService(getBeanMapper().map(entity.getCarRentalService(), CarRentalServiceEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<CarReservationCto> findCarReservationCtos(CarReservationSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<CarReservationEntity> carreservations = getCarReservationDao().findCarReservations(criteria);
    List<CarReservationCto> ctos = new ArrayList<>();
    for (CarReservationEntity entity : carreservations.getResult()) {
      CarReservationCto cto = new CarReservationCto();
      cto.setCarReservation(getBeanMapper().map(entity, CarReservationEto.class));
      cto.setCarRentalInfo(getBeanMapper().map(entity.getCarRentalInfo(), CarRentalInfoEto.class));
      cto.setCarRentalService(getBeanMapper().map(entity.getCarRentalService(), CarRentalServiceEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<CarReservationCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

  @Override
  public RentalCarStatusEto findRentalCarStatus(Long id) {

    LOG.debug("Get RentalCarStatus with id {} from database.", id);
    return getBeanMapper().map(getRentalCarStatusDao().findOne(id), RentalCarStatusEto.class);
  }

  @Override
  public PaginatedListTo<RentalCarStatusEto> findRentalCarStatusEtos(RentalCarStatusSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<RentalCarStatusEntity> rentalcarstatuss = getRentalCarStatusDao().findRentalCarStatuss(criteria);
    return mapPaginatedEntityList(rentalcarstatuss, RentalCarStatusEto.class);
  }

  @Override
  public boolean deleteRentalCarStatus(Long rentalCarStatusId) {

    RentalCarStatusEntity rentalCarStatus = getRentalCarStatusDao().find(rentalCarStatusId);
    getRentalCarStatusDao().delete(rentalCarStatus);
    LOG.debug("The rentalCarStatus with id '{}' has been deleted.", rentalCarStatusId);
    return true;
  }

  @Override
  public RentalCarStatusEto saveRentalCarStatus(RentalCarStatusEto rentalCarStatus) {

    Objects.requireNonNull(rentalCarStatus, "rentalCarStatus");
    RentalCarStatusEntity rentalCarStatusEntity = getBeanMapper().map(rentalCarStatus, RentalCarStatusEntity.class);

    // initialize, validate rentalCarStatusEntity here if necessary
    RentalCarStatusEntity resultEntity = getRentalCarStatusDao().save(rentalCarStatusEntity);
    LOG.debug("RentalCarStatus with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, RentalCarStatusEto.class);
  }

  /**
   * Returns the field 'rentalCarStatusDao'.
   *
   * @return the {@link RentalCarStatusDao} instance.
   */
  public RentalCarStatusDao getRentalCarStatusDao() {

    return this.rentalCarStatusDao;
  }

  @Override
  public RentalCarStatusCto findRentalCarStatusCto(Long id) {

    LOG.debug("Get RentalCarStatusCto with id {} from database.", id);
    RentalCarStatusEntity entity = getRentalCarStatusDao().findOne(id);
    RentalCarStatusCto cto = new RentalCarStatusCto();
    cto.setRentalCarStatus(getBeanMapper().map(entity, RentalCarStatusEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<RentalCarStatusCto> findRentalCarStatusCtos(RentalCarStatusSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<RentalCarStatusEntity> rentalcarstatuss = getRentalCarStatusDao().findRentalCarStatuss(criteria);
    List<RentalCarStatusCto> ctos = new ArrayList<>();
    for (RentalCarStatusEntity entity : rentalcarstatuss.getResult()) {
      RentalCarStatusCto cto = new RentalCarStatusCto();
      cto.setRentalCarStatus(getBeanMapper().map(entity, RentalCarStatusEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<RentalCarStatusCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

}
