#!groovy

timeout(time: 60, unit: 'MINUTES'){
	node {
	  // Invoke OpenShift Build to package the carrental.war file
	  // with the S2I image.
	  
	  stage('Checkout') {
        checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[url: 'https://dgauda@bitbucket.org/dgauda/car-rental-simulator.git']]])
      }
	  def version = readMavenPom().getVersion()
	  
	  stage('OpenShift Build') {
		sh "oc project dev"
		openshiftBuild apiURL: '', authToken: '', bldCfg: 'carrentalapp', waitTime: '1800000', checkForTriggeredDeployments: 'false', namespace: 'dev', verbose: 'false'
		openshiftVerifyBuild apiURL: '', authToken: '', bldCfg: 'carrentalapp', checkForTriggeredDeployments: 'false', namespace: 'dev', verbose: 'false'
		openshiftTag alias: 'false', apiURL: '', authToken: '', destStream: 'carrentalapp', destTag: "dev-${version}", destinationAuthToken: '', destinationNamespace: 'dev', namespace: 'dev', srcStream: 'carrentalapp', srcTag: 'latest', verbose: 'false'
	  }
	  
	  // Deploy the created S2I Image
	  stage('OpenShift Deployment') {
		sh "oc patch dc carrentalapp --patch '{\"spec\": { \"triggers\": [ { \"type\": \"ImageChange\", \"imageChangeParams\": { \"containerNames\": [ \"carrentalapp\" ], \"from\": { \"kind\": \"ImageStreamTag\", \"namespace\": \"dev\", \"name\": \"carrentalapp:dev-$version\"}}}]}}' -n dev"
		openshiftDeploy apiURL: '', authToken: '', depCfg: 'carrentalapp', namespace: 'dev', verbose: 'false', waitTime: ''
		openshiftScale apiURL: '', authToken: '', depCfg: 'carrentalapp', namespace: 'dev', replicaCount: '1', verbose: 'false', verifyReplicaCount: 'false'
		openshiftVerifyDeployment apiURL: '', authToken: '', depCfg: 'carrentalapp', namespace: 'dev', replicaCount: '1', verbose: 'false', verifyReplicaCount: 'false', waitTime: ''
	  }
	  stage('Verify Service') {
		// Starting the carrental application takes a long time.
		// Retrying 5 times to contact the service to make sure
		// we give enough time for the container to be fully ready.
		retry(5) {
		  openshiftVerifyService apiURL: '', authToken: '', namespace: 'dev', svcName: 'carrentalapp', verbose: 'false'
		}
	  }
	}
}
